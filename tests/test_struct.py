import unittest
from own.own_struct.struct import dict_to_tab, dict_to_tuple

class Test_Own_CSV(unittest.TestCase):

    def test_dict_to_tab(self):
        dict1 = {
            "dat1":98,
            "balp":"98"
        }
        dict2 = {
            "dat":12,
            "dit":"456",
            "dict":{"data1":9, "data2":"Bloup"}
        }
        tab1 = [98, "98"]
        tab2 = [12, 456, {"data1":9, "data2":"Bloup"}]
        tab3 = [98, 98]
        self.assertEqual(tab1, dict_to_tab(dict1))
        self.assertEqual(tab2, dict_to_tab(dict2, int))
        self.assertEqual(tab3, dict_to_tab(dict1, int, ["balp"]))
    
    def test_dict_to_tuple(self):
        dict1 = {
            "dat1":98,
            "balp":"98"
        }
        dict2 = {
            "dat":12,
            "dit":"456",
            "dict":{"data1":9, "data2":"Bloup"}
        }
        tuple1 = (98, "98")
        tuple2 = (12, 456, {"data1":9, "data2":"Bloup"})
        tuple3 = (98, 98)
        self.assertEqual(tuple1, dict_to_tuple(dict1))
        self.assertEqual(tuple2, dict_to_tuple(dict2, int))
        self.assertEqual(tuple3, dict_to_tuple(dict1, int, ["balp"]))