# Module own_struct

## Description

Ce module nous permet de remanier des dictionnaire en un autre type de structure, en ne conservant que les valeurs de chaque index (Les indexs sont donc perdus).

### Fonctions

Dans ce module on possède que 2 fonctions :

* dict_to_tab() qui convertit un dictionnaire en tableau (ou liste vous avez compris le type).
* dict_to_tuple() qui convertit un dictionnaire en tuple.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_struct. On y teste les deux fonction précédente avec 2 lots de données et où l'on teste tout les options de nos méthodes.
