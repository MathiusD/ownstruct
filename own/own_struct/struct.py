"""
    Fonction dict_to_tab
    %       Fonction qui nous renvoie sous forme de tableau, le dictionnaire
            passé en argument.
            Cette dernière tentera de caster les chaines de caractères en
            un type passé en tant que valeur de Cast.
            Si Values_Cast est renseigné on ne tentera de cast en type passé
            en tant que valeur de Cast que pour les champs portant l'un des
            noms au sein de Values_Cast.
    %IN     dictio: dictionnaire à convertir
            Cast: type pour le cast si souhaité
            Values_Cast: Liste de Valeur à explicitement convertir
    %OUT    tab: tableau équivalent
"""

def dict_to_tab(dictio, Cast = None, Values_Cast = None):
    tab = []
    for row in dictio:
        if Values_Cast == None:
            if Cast == None:
                tab.append(dictio[row])
            else:
                if type(dictio[row]) == str:
                    pas = False
                    try:
                        tab.append(Cast(dictio[row]))
                        pas = True
                    finally:
                        if pas == False:
                            tab.append(dictio[row])
                else:
                    tab.append(dictio[row])
        else:
            if row in Values_Cast:
                tab.append(Cast(dictio[row]))
            else:
                tab.append(dictio[row])
    return tab

"""
    Fonction dict_to_tuple
    %       Fonction qui nous renvoie sous forme de tuple, le dictionnaire
            passé en argument.
            Cette dernière tentera de caster les chaines de caractères en
            un type passé en tant que valeur de Cast.
            Si Values_Cast est renseigné on ne tentera de cast en type passé
            en tant que valeur de Cast que pour les champs portant l'un des
            noms au sein de Values_Cast.
    %IN     dictio: dictionnaire à convertir
            Cast: type pour le cast si souhaité
            Values_Cast: Liste de Valeur à explicitement convertir
    %OUT    tuples: tuple équivalent
"""

def dict_to_tuple(dictio, Cast = None, Values_Cast = None):
    tuples = tuple(dict_to_tab(dictio, Cast, Values_Cast))
    return tuples